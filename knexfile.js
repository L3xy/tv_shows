// Update with your config settings.

module.exports = {

  test: {
    client: 'pg',
    connection: 'postgres://cvjpjjwp:QLSoGuw9-nxfoLkiTlOXeKGUfOu9iQO7@stampy.db.elephantsql.com:5432/cvjpjjwp',
    migrations: {
      directory:__dirname + '/db/migrations'
    },
    seeds: {
      directory: __dirname + '/db/seeds/test'
    }
  },
  development: {
    client: 'pg',
    connection: 'postgres://cqqrgfue:9Q46U2ezYbijVPggSyKtdXnBNKbBslHm@stampy.db.elephantsql.com:5432/cqqrgfue',
    migrations: {
      directory:__dirname + '/db/migrations'
    },
    seeds: {
      directory: __dirname + '/db/seeds/development'
    }
  },
  production: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
    migrations: {
      directory: __dirname + '/db/migrations'
    },
    seeds: {
      directory: __dirname + '/db/seeds/production'
    }
  }
};
